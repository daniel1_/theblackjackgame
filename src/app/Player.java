package app;

import java.util.ArrayList;

public class Player {
    private final String name;
    private final Strategy strategy;
    private ArrayList<Card> cardList;
    private PlayerStatus playerStatus;
    private int cardTotal = 0;


    public Player(String name) {
        this(name, Strategy.DEFAULT);
    }

    public Player(String name, Strategy strategy) {
        this.name = name;
        this.strategy = strategy;
        this.cardList = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Card> getCardList() {
        return this.cardList;
    }

    public PlayerStatus getPlayerStatus() {
        return this.playerStatus;
    }

    public void setPlayerStatus(PlayerStatus playerStatus) {
        this.playerStatus = playerStatus;
    }

    public void receiveCard(Card card) {
        this.cardList.add(card);
        this.cardTotal += card.getCardValue();
    }

    public int getCardTotal() {
        return this.cardTotal;
    }

    public Strategy getStrategy() {
        return strategy;
    }

//    public void
}
