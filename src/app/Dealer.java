package app;

import java.util.*;

public class Dealer {
    private Deque<Card> cardDeck;
    private boolean active;

    public Dealer() {
        this.generateACardDeck();
    }

    public Deque<Card> getCardDeck() {
        return this.cardDeck;
    }

    private void generateACardDeck() {
        List<Card> categoryOfCards = new ArrayList<>();

        categoryOfCards.addAll(this.generateCategoryOfCards(CardType.HEARTS));
        categoryOfCards.addAll(this.generateCategoryOfCards(CardType.DIAMOND));
        categoryOfCards.addAll(this.generateCategoryOfCards(CardType.SPADES));
        categoryOfCards.addAll(this.generateCategoryOfCards(CardType.CLUBS));

        Collections.shuffle(categoryOfCards);

        this.cardDeck = new ArrayDeque<>(categoryOfCards);

        System.out.println(this.cardDeck);
    }

    private String intToString(int num) {
        return switch (num) {
            case 2 -> "TWO";
            case 3 -> "THREE";
            case 4 -> "FOUR";
            case 5 -> "FIVE";
            case 6 -> "SIX";
            case 7 -> "SEVEN";
            case 8 -> "EIGHT";
            case 9 -> "NINE";
            case 10 -> "TEN";
            case 11 -> "ACE";
            case 12 -> "JACK";
            case 13 -> "QUEEN";
            case 14 -> "KING";
            default -> "";
        };
    }

    private List<Card> generateCategoryOfCards(CardType cardType) {
        List<Card> categoryOfCards = new ArrayList<>();

        for (int i = 2; i <= 10; i++) {
            categoryOfCards.add(new Card(cardType, i, intToString(i)));
        }
//        Refactor
        categoryOfCards.add(new Card(cardType, 10, intToString(12)));
        categoryOfCards.add(new Card(cardType, 10, intToString(13)));
        categoryOfCards.add(new Card(cardType, 10, intToString(14)));
        categoryOfCards.add(new Card(cardType, 11, intToString(11)));

        return categoryOfCards;
    }

    private void giveCardToPlayer(ArrayList<Player> players) {
        for (Player player : players) {
            if (player.getPlayerStatus() == PlayerStatus.HIT)
                player.receiveCard(this.cardDeck.pop());

        }
    }

    private void evaluatePlayerStatus(ArrayList<Player> players) {
        final int MAXIMUM_HIT_NUMBER = 16;
        final int MAXIMUM_STICK_NUMBER = 20;
        final int MINIMUM_STICK_NUMBER = 17;
        final int GO_BUST_NUMBER = 21;
        final int BLACK_JACK = 21;

        int numberOfPlayer = players.size();
        int numOfPlayerThatCannotReceive = 0;

        for (Player player : players) {
            if (numOfPlayerThatCannotReceive == numberOfPlayer - 1) {
                this.active = false;
                break;
            }

            boolean canGameEnd = BLACK_JACK == player.getCardTotal();
            boolean canPlayHit = player.getCardTotal() <= MAXIMUM_HIT_NUMBER;
            boolean canPlayerBust = player.getCardTotal() >= GO_BUST_NUMBER;
            boolean canPlayerStick = player.getCardTotal() >= MINIMUM_STICK_NUMBER && player.getCardTotal() <= MAXIMUM_STICK_NUMBER;

            if (canPlayHit) {
                player.setPlayerStatus(PlayerStatus.HIT);
            } else if (canPlayerStick) {
                player.setPlayerStatus(PlayerStatus.STICK);
                numOfPlayerThatCannotReceive++;
            } else if (canGameEnd) {
                this.active = false;
            } else if (canPlayerBust) {
                player.setPlayerStatus(PlayerStatus.BUST);
                numOfPlayerThatCannotReceive++;
            }
        }
    }

    public void startGame(ArrayList<Player> players) {
        this.active = true;
        this.giveCardToPlayer(players);
        this.giveCardToPlayer(players);
        this.evaluatePlayerStatus(players);

        while (this.active && !this.cardDeck.isEmpty()) {
            this.giveCardToPlayer(players);
            this.evaluatePlayerStatus(players);
        }

        // List player details when game ends
        for (Player p : players) {
            System.out.print(p.getName() + " is dealt" + "\n");
            for (int i = 0; i < p.getCardList().size(); i++) {
                System.out.println(p.getCardList().get(i) + "\r");
            }
            System.out.println("\r");
        }
    }
}

// TODO: log status at each round.
// TODO: check display winner
// TODO: Discuss if to move evaluate to player class