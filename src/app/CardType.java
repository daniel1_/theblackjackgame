package app;

public enum CardType {
    CLUBS("clubs"),
    SPADES("spades"),
    DIAMOND("diamond"),
    HEARTS("hearts");
    private final String name;

    CardType(String name) {
        this.name = name;
    }
}
