package app;

public enum Strategy {
    ALWAYS_STICK,
    ALWAYS_HIT,
    RISK_CALCULATOR,
    DEFAULT;
}