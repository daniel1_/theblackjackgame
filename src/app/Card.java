package app;

public class Card {

    private final CardType cardType;
    private final String cardValueString;
    private final int cardValue;

    public Card(CardType cardType, int cardValue, String cardValueString) {
        this.cardType = cardType;
        this.cardValue = cardValue;
        this.cardValueString = cardValueString;
    }

    public CardType getCardType() {
        return this.cardType;
    }

    public int getCardValue() {
        return this.cardValue;
    }

    public String getCardValueString() {
        return this.cardValueString;
    }

    @Override
    public String toString() {
        return this.cardValue + " " + this.cardValueString + " " + this.cardType.name();
    }
}