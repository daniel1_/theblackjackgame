package app;

import java.util.ArrayList;
import java.util.Scanner;

public class BlackJackGame {

    private final int DEFAULT_NUMBER_OF_PLAYERS = 3;

    public static Strategy getCorrespondingStrategy(String strategy) {
        return switch (strategy) {
            case "always-hit" -> Strategy.ALWAYS_HIT;
            case "always-stick" -> Strategy.ALWAYS_STICK;
            case "risk-calculator" -> Strategy.RISK_CALCULATOR;
            default -> Strategy.DEFAULT;
        };
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        Scanner scanner = new Scanner(System.in);

        String mech[] = {"Pharaoh", "Riffle", "Default"};
        System.out.println("Choose a shuffling Mechanism: ");
        System.out.println("[1] " + mech[0] + " Shuffling Mechanism");
        System.out.println("[2] " + mech[1] + " Shuffling Mechanism");
        System.out.println("[3] " + mech[2] + " Shuffling Mechanism");

        int option = scanner.nextInt();
        System.out.println(mech[(option > 0 && option <= 3) ? option - 1 : 2] + " Shuffling Mechanism");

        ArrayList<Player> players = new ArrayList<>();

        int numberOfParams = args.length;

        if (numberOfParams > 1) {
            int count = 1;
            //Creating the player
            for (int i = 0; i < numberOfParams; i += 2) {
                String name = "Player " + (count++);
                Strategy strategy = getCorrespondingStrategy(args[i + 1]);
                players.add(new Player(name, strategy));
            }
        } else {
            BlackJackGame blackJackGame = new BlackJackGame();

            int numberOfPlayer = blackJackGame.getNumOfPlayerFromInput();
            blackJackGame.createPlayers(players, numberOfPlayer);

            Dealer dealer = new Dealer();
            dealer.startGame(players);
        }
    }

    public int getNumOfPlayerFromInput() {
        final int MINIMUM_NUMBER_PLAYER = 2;
        final int MAXIMUM_NUMBER_PLAYER = 6;
        boolean isNotValid = true;
        int numOfPlayers = DEFAULT_NUMBER_OF_PLAYERS;

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Enter number of players");
            numOfPlayers = scanner.nextInt();

            isNotValid = numOfPlayers < MINIMUM_NUMBER_PLAYER || numOfPlayers > MAXIMUM_NUMBER_PLAYER;

            if (isNotValid)
                System.out.println("Enter number between 1 to 6");
        } while (isNotValid);

        return numOfPlayers;
    }

    private void createPlayers(ArrayList<Player> players, int numberOfPlayer) {

        for (int i = 0; i < numberOfPlayer; i++) {
            players.add(new Player("Player " + (i + 1)));
        }
    }

}
