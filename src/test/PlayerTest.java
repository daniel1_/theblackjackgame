package test;

import app.Card;
import app.CardType;
import app.Player;

import static org.junit.jupiter.api.Assertions.*;

import app.PlayerStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {
    private Player player;

    @BeforeEach
    void setup() {
        player = new Player("Player 1");
    }

    @Test
    void getNameTest() {
        assertEquals("Player 1", player.getName());
    }

    @Test
    void setPlayerStatusTest() {
        player.setPlayerStatus(PlayerStatus.HIT);
        PlayerStatus actual = player.getPlayerStatus();
        assertEquals(PlayerStatus.HIT, actual);
    }

    @Test
    void receiveCardTest() {
        assertEquals(0, player.getCardList().size());

        player.receiveCard(new Card(CardType.HEARTS, 2, "TWO"));

        String expected = new Card(CardType.HEARTS, 2, "TWO").toString();

        assertEquals(1, player.getCardList().size());
        assertEquals(expected, player.getCardList().get(0).toString());

    }

    @Test
    void getCardTotalTest() {
        player.receiveCard(new Card(CardType.HEARTS, 3, "THREE"));
        assertEquals(3, player.getCardTotal());
    }
}