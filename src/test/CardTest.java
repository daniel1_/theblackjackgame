package test;

import app.Card;
import app.CardType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    private Card card;

    @BeforeEach
    void setup() {
        card = new Card(CardType.HEARTS, 4, "FOUR");
    }

    @Test
    void getCardType() {
        assertEquals(CardType.HEARTS, card.getCardType());
    }

    @Test
    void getCardValue() {
        assertEquals(4, card.getCardValue());
    }

    @Test
    void getCardValueString() {
        assertEquals("FOUR", card.getCardValueString());
    }

    @Test
    void testToString() {
        assertEquals("4 FOUR HEARTS", card.toString());
    }
}